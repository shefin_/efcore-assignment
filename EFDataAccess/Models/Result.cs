﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDataAccess.Models
{
    public class Result
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(10)]
        public string Roll { get; set; }

        [Required]
        [MaxLength(20)]
        public string Subject { get; set; }

        [Required]
        public float GP { get; set; }
    }
}
