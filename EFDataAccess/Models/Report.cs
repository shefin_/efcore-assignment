﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDataAccess.Models
{
    public class Report
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MaxLength(10)]
        public string Roll { get; set; }

        [Required]
        public float CG { get; set; }

        public Report() { }
        public Report(string name, string roll, float cg)
        {
            Name = name;
            Roll = roll;
            CG = cg;
        }
    }
}
