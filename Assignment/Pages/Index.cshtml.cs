﻿using EFDataAccess.DataAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Text.Json;
using System;
using EFDataAccess.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Assignment.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly ReportContext _db;
        private List<Report> ListReports;

        public IndexModel(ILogger<IndexModel> logger, ReportContext db)
        {
            _logger = logger;
            _db = db;
        }

        public void OnGet()
        {
            LoadSampleData();
            GenerateResults();
            SaveResultsToDb();
        }

        private void LoadSampleData()
        {
            if (_db.Students.Count() == 0)
            {
                string file = System.IO.File.ReadAllText("StudentData.json");
                var students = JsonSerializer.Deserialize<List<Student>>(file);
                _db.Students.AddRange(students);
            }

            if(_db.Results.Count() == 0)
            {
                string file = System.IO.File.ReadAllText("ResultData.json");
                var results = JsonSerializer.Deserialize<List<Result>>(file);
                _db.Results.AddRange(results);
            }

            _db.SaveChanges();
        }

        private void GenerateResults()
        {
            var Students = _db.Students.Select(student => new
                                            {
                                                Name = student.Name,
                                                Roll = student.Roll
                                            }).ToList();

            var Results = _db.Results.Select(result => new
                                    {
                                        Roll = result.Roll,
                                        Subject = result.Subject,
                                        GP = result.GP
                                    }).ToList();

            var StudentResultGroup = Students.GroupJoin(Results,
                                                            student => student.Roll,
                                                            result => result.Roll,
                                                            (student, results) => new
                                                            {
                                                                Name = student.Name,
                                                                Roll = student.Roll,
                                                                Results = results
                                                            });

            ListReports = new List<Report>();

            foreach (var ResultInfo in StudentResultGroup)
            {
                string Name = ResultInfo.Name;
                string Roll = ResultInfo.Roll;

                float SummationGP = 0;
                foreach (var Result in ResultInfo.Results)
                {
                    SummationGP += Result.GP;
                }

                float CG = SummationGP / ResultInfo.Results.Count();

                ListReports.Add(new Report(Name, Roll, CG));
            }

            ListReports = ListReports.OrderByDescending(Info => Info.CG)
                                             .ThenBy(Info => Info.Roll)
                                             .ThenBy(Info => Info.Name)
                                             .ToList<Report>();
        }

        private void SaveResultsToDb()
        {
            DeleteAllReports();

            _db.Reports.AddRange(ListReports);
            _db.SaveChanges();
        }

        private void DeleteAllReports()
        {
            List <Report> Reports = _db.Reports.ToList<Report>();
            _db.Reports.RemoveRange(Reports);
        }
    }
}